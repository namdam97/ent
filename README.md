# Installation
    go install entgo.io/ent/cmd/ent@latest

# Create your first Schema
    go run entgo.io/ent/cmd/ent init <User>

# Run go generate from the root directory of the project as follows:
    go generate ./ent
    